import {obj} from "./object.js"

// single specific calls
obj.climate.functions.example1()
obj.climate.functions.example2()

// iteratively calling all funcs
for (const functionName in obj.climate.functions) {
  if (typeof obj.climate.functions[functionName] === 'function') {
    obj.climate.functions[functionName]();
  }
}
